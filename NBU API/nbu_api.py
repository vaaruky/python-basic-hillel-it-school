import requests
import time

separator = '*' * 20
local_time = time.strftime('%d %b %Y %H-%M-%S')
nbu_url = 'https://bank.gov.ua/NBU_Exchange/exchange_site?order=desc&sort=exchangedate&json'
file_extension = ['.txt', '.doc', '.docx', '.odt']


def get_file_name():
    """
    Function for getting name of file
    Returns (str): File name
    """
    filename = input('Enter file name: ')
    while True:
        filename_extension = input('Enter file extension (.txt, .doc, .docx, .odt): ')
        if filename_extension not in file_extension:
            print('Enter right extension')
            continue
        break
    return filename + filename_extension


def get_rates(url):
    """
    Function for getting json from response
    Args:
        url (str): API url
    Returns : JSON
    """
    try:
        r = requests.get(nbu_url)
    except requests.exceptions.RequestException as e:
        raise SystemExit(e)
    else:
        return r.json()


def writing_rates(currency_list, file_name):
    """
    Function for writing into the file
    Args:
        currency_list (dict): Dict from JSON
        file_name (str): Name of file
    """
    with open(file_name, 'a') as file:
        file.write(f'{separator}\n{local_time}\n{separator}\n')
        for index, currency in enumerate(currency_list, start=1):
            file.write(
                f'{index}. {currency["cc"]}  {currency["units"]:5.0f}  {currency["txt"]} -- {currency["rate"]} UAH\n')


def main():
    rates = get_rates(nbu_url)
    file_name = get_file_name()
    writing_rates(rates, file_name)


main()
