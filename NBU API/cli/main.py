from lib import default_date, default_currency, currency_list, api_url, get_json, get_file, app
from datetime import datetime


@app.command()
def get_url(date: datetime = default_date, currency: str = default_currency, save: bool = False):
    dt = date.strftime('%Y%m%d')
    if currency.upper() not in currency_list:
        print(f'Your currency \'{currency.upper()}\' is missing!')
    else:
        print(f'Your currency is {currency.upper()}')
    nbu_url = f'{api_url}/NBU_Exchange/exchange_site?start={dt}&end={dt}&valcode={currency}&sort=exchangedate&order=desc&json'
    rates = get_json(nbu_url)
    if save:
        get_file(rates)


if __name__ == '__main__':
    app()

