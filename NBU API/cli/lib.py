from datetime import datetime
import typer
import requests
import time

app = typer.Typer()
api_url = 'https://bank.gov.ua'
default_date = typer.Argument(datetime.now(), formats=['%Y%m%d'])
currency_list = ['AUD', 'DZD', 'AMD', 'CAD', 'CNY', 'HRK', 'CZK', 'DKK', 'HKD', 'HUF', 'INR', 'IDR', 'IRR', 'IQD',
                 'ILS', 'JPY', 'KZT', 'KRW', 'KGS', 'LBP', 'LYD', 'MYR', 'MXN', 'MDL', 'MAD', 'NZD', 'NOK', 'PKR',
                 'RUB', 'SGD', 'VND', 'ZAR', 'SEK', 'CHF', 'THB', 'AED', 'TND', 'EGP', 'GBP', 'USD', 'UZS', 'BYN',
                 'TMT', 'RSD', 'RON', 'TRY', 'TJS', 'BGN', 'GEL', 'PLN', 'BRL', 'DOP', 'XAU', 'XDR', 'XAG', 'XPT',
                 'XPD', 'EUR', 'AZN', 'SAR', 'TWD', 'BDT']
default_currency = typer.Argument('USD')


def get_json(url):
    try:
        r = requests.get(url)
    except requests.exceptions.RequestException as e:
        raise SystemExit(e)

    rates = [f'{currency["units"]}  {currency["txt"]} - {currency["rate"]} UAH'
             for currency in r.json()]
    print(rates)
    return r.json()


def get_file(list):
    rates = [f'{item["units"]} {item["txt"]} - {item["rate"]} UAH' for item in list]
    local_time = time.strftime('%d %b %Y %H-%M-%S')
    with open('currency.txt', 'a') as file:
        file.write(f'{local_time} {rates}\n')
