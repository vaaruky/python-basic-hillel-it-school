from enum import Enum
import time


class FileExtensions(Enum):
    txt = '.txt'
    doc = '.doc'
    docx = '.docx'
    odt = '.odt'


class File:
    def __init__(self, name, extension):
        self.name = name
        try:
            self.extension = FileExtensions(extension).value
        except ValueError:
            raise AttributeError(f'No such extension!\nAvailable extensions - '
                                 f'{FileExtensions.__members__.values()}')
        self.full_name = self.name + self.extension
        self.local_time = time.strftime('%d %b %Y %H-%M-%S')
        self._write_local_time()

    def _write_local_time(self):
        separator = '*' * 20
        with open(self.full_name, 'a', encoding='utf-8') as file:
            file.write(f'{separator}\n{self.local_time}\n{separator}\n')

    def write_list(self, some_list):
        with open(self.full_name, 'a', encoding='utf-8') as file:
            for index, item in enumerate(some_list, start=1):
                file.write(f'{index}. {item}\n')
