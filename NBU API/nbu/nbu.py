from request import Request


class Nbu(Request):
    def __init__(self):
        super().__init__('https://bank.gov.ua/')

    def get_currency_list(self):
        url = f'{self.base_url}NBU_Exchange/exchange_site?order=desc&sort=exchangedate&json'
        return self.get_json(url)
