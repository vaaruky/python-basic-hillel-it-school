from file import File
from nbu import Nbu


def main():
    nbu = Nbu()
    currency_list = nbu.get_currency_list()
    rates_list = [f'{currency["cc"]}  {currency["units"]:5.0f}  {currency["txt"]} -- {currency["rate"]} UAH'
                  for currency in currency_list]
    file = File('currency_rates', '.txt')
    file.write_list(rates_list)


main()
