import requests


class Request:
    def __init__(self, base_url):
        self.base_url = base_url

    @staticmethod
    def get_json(url):
        try:
            response = requests.get(url)
        except requests.exceptions.RequestException as e:
            raise SystemExit(e)
        else:
            return response.json()
